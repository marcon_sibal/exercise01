<?php

use App\Constants\OrderStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')
                ->unsigned();

            $table->integer('status')
                ->length(3)
                ->unsigned()
                ->default(OrderStatus::UNDER_REVIEW);

            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('status')
                ->references('id')
                ->on('order_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign('orders_user_id_foreign');
            $table->dropIndex('orders_user_id_foreign');

            $table->dropForeign('orders_status_foreign');
            $table->dropIndex('orders_status_foreign');
        });

        Schema::dropIfExists('orders');
    }
};
