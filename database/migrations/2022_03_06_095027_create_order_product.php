<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_product', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('order_id')
                ->unsigned();
            $table->bigInteger('product_id')
                ->unsigned();
            $table->bigInteger('product_variation_id')
                ->unsigned();
            $table->timestamps();

            $table->foreign('order_id')
                ->references('id')
                ->on('orders');

            $table->foreign('product_id')
                ->references('id')
                ->on('products');

            $table->foreign('product_variation_id')
                ->references('id')
                ->on('product_variations');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_product', function (Blueprint $table) {
            $table->dropForeign('order_product_order_id_foreign');
            $table->dropIndex('order_product_order_id_foreign');

            $table->dropForeign('order_product_product_id_foreign');
            $table->dropIndex('order_product_product_id_foreign');

            $table->dropForeign('order_product_product_variation_id_foreign');
            $table->dropIndex('order_product_product_variation_id_foreign');
        });

        Schema::dropIfExists('order_products');
    }
};
