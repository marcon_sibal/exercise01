<?php

namespace Database\Seeders;

use App\Constants\OrderStatus;
use App\Models\OrderStatus as OrderStatusModel;
use Illuminate\Database\Seeder;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (OrderStatus::NAMES as $id => $name) {
            OrderStatusModel::insert([
                'id'    => $id,
                'name'  => $name,
            ]);
        }
    }
}
