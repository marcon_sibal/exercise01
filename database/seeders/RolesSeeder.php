<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Constants\UserRoles;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (UserRoles::NAMES as $id => $name) {
            Role::insert([
                'id'    => $id,
                'name'  => $name,
            ]);
        }
    }
}
