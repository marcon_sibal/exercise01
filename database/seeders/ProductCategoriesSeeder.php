<?php

namespace Database\Seeders;

use App\Constants\OrderStatus;
use App\Constants\ProductCategories as ProductCategoriesConstants;
use App\Models\ProductCategory;
use Illuminate\Database\Seeder;

class ProductCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (ProductCategoriesConstants::NAMES as $id => $name) {
            ProductCategory::insert([
                'id'    => $id,
                'name'  => $name,
            ]);
        }
    }
}
