<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $productsData = [
            [
                'name'                  => 'Connecto',
                'details'               => 'is a connectivity product.',
                'product_category_id'   => 1,
                'price'                 => 11.00,
            ],
            [
                'name'                  => 'Securo',
                'details'               => 'is a secureity product.',
                'product_category_id'   => 2,
                'price'                 => 12.00,
            ],
            [
                'name'                  => 'Dineso',
                'details'               => 'is a DNS product',
                'product_category_id'   => 3,
                'price'                 => 14.50,
            ],
            [
                'name'                  => 'Joseo',
                'details'               => 'is a hosting product',
                'product_category_id'   => 4,
                'price'                 => 9.50,
            ]
        ];

        Product::insert($productsData);
    }

}
