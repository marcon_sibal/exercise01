<?php

namespace Database\Seeders;

use App\Constants\ProductCategories;
use App\Models\Product;
use App\Models\ProductVariation;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ProductVariationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $productVariationsData = [
            [
                'name'                  => '12',
                'details'               => '12 months',
                'price'                 => 12.00,
            ],
            [
                'name'                  => '24',
                'details'               => '24 moths',
                'price'                 => 24.00,
            ],
            [
                'name'                  => '36',
                'details'               => '36 months',
                'price'                 => 36.00,
            ],
            [
                'name'                  => '48',
                'details'               => '48 months',
                'price'                 => 48.00,
            ],
            [
                'name'                  => '60',
                'details'               => '60 months',
                'price'                 => 60.00,
            ]
        ];

        foreach (ProductCategories::NAMES as $id => $name) {
            foreach ($productVariationsData as $productVariation) {
                $pvData = $productVariation;
                $pvData['product_id'] = $id;

                ProductVariation::insert($pvData);
            }
        }
    }

}
