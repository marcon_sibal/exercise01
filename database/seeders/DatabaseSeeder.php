<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Database\Seeders\RolesSeeder;
use Database\Seeders\OrderStatusSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RolesSeeder::class,
            OrderStatusSeeder::class,
            ProductCategoriesSeeder::class,
            ProductSeeder::class,
            ProductVariationsSeeder::class,
        ]);

        // make 10 users for demo
        User::factory()->count(10)->create();
    }
}
