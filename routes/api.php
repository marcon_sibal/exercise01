<?php

use App\Http\Controllers\Api\CustomerController;
use App\Http\Controllers\Api\OrderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// available to pulic for customer registration.
Route::post('customers', [CustomerController::class, 'store']);

// @note: since this is not using authentication the user id has to be exposed for demo
// normally I would grab the authenticated user
Route::patch('customers/{user}', [CustomerController::class, 'update']);
Route::get('customers/{user}', [CustomerController::class, 'show']);
Route::delete('customers/{user}', [CustomerController::class, 'destroy']);


Route::post('orders', [OrderController::class, 'store']);
Route::get('orders/{order}', [OrderController::class, 'show']);
Route::patch('orders/{order}/update-status', [OrderController::class, 'updateOrderStatus']);
Route::patch('orders/{order}/remove-product', [OrderController::class, 'removeOrderProduct']);
Route::post('orders/{order}/add-product', [OrderController::class, 'addOrderProduct']);
Route::delete('orders/{order}', [OrderController::class, 'destroy']);


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
