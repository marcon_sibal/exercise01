<?php
namespace App\Constants;

class ProductCategories {
    const CONNECTIVITY = 1;
    const SECURITY = 2;
    const DNS = 3;
    const HOSTING = 4;

    const NAMES = [
        self::CONNECTIVITY => 'Connectivity',
        self::SECURITY => 'Security',
        self::DNS => 'DNS',
        self::HOSTING => 'Hosting',
    ];
}