<?php
namespace App\Constants;

class OrderStatus {
    const UNDER_REVIEW = 1;
    const IN_PROGRESS = 2;
    const COMPLETED = 3;

    const NAMES = [
        self::UNDER_REVIEW  => 'Under review',
        self::IN_PROGRESS   => 'In Progress',
        self::COMPLETED     => 'Completed',
    ];
}