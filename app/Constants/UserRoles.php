<?php
namespace App\Constants;

class UserRoles {
    const CUSTOMER = 1;

    const NAMES = [
        self::CUSTOMER => 'Customer',
    ];
}