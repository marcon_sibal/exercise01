<?php

namespace App\Models;

use App\Models\ProductVariation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'datails',
        'product_category_id',
        'price',
    ];

    public function productVariations()
    {
        return $this->hasMany(ProductVariation::class);
    }
}
