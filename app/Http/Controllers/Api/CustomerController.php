<?php

namespace App\Http\Controllers\Api;

use Throwable;
use App\Models\User;
use App\Constants\UserRoles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Api\Base\Controller;

class CustomerController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customerData = $request->validate([
            'email'         => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
            'password'      => 'required|confirmed|min:6',

            'name'          => 'required|string',
            'firstname'     => 'required|string',
            'lastname'      => 'required|string',
        ]);

        // insert role data
        $customerData['role_id'] = UserRoles::CUSTOMER;

        // for demo -- this is simply inserting the customers user data.
        // depending on the registration process - this method would need to insert records
        // in multiple table and may need to use DB::transaction.

        // insert user
        if (User::create($customerData)) {
            return $this->formatResponse(null, 'Success', 201);
        };

        return $this->formatResponse(null, 'Failed', 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return $this->formatResponse($user, 'Success', 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $customerData = $request->validate([
            'password'      => 'required|confirmed|min:6',

            'name'          => 'required|string',
            'firstname'     => 'required|string',
            'lastname'      => 'required|string',
        ]);

        // @note - when using sanctum you can grab the user by
        // $user = auth()->user();
        // udpate user
        if ($user->update($customerData)) {
            return $this->formatResponse(null, 'Success', 202);
        };

        return $this->formatResponse(null, 'Failed', 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if ($user->delete()) {
            return $this->formatResponse(null, 'Sucess', 200);
        }

        return $this->formatResponse(null, 'Failed', 400);
    }
}
