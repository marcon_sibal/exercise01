<?php

namespace App\Http\Controllers\Api\Base;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * Formats response to a standard format.
     *
     * @param [type] $data
     * @param string|null $message
     * @param int $code - http code - default to fail code to avoid accidental success when code is ommited.
     * @return \Illuminate\Http\JsonResponse
     */
    public function formatResponse($data, string $message = null, int $code = 400)
    {
        return response()->json([
            'success' => $code >= 200 && $code < 300,
            'message' => $message,
            'data' => $data,
        ], $code);
    }
}
