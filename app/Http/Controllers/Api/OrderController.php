<?php

namespace App\Http\Controllers\Api;

use App\Models\Order;
use App\Models\OrderProduct;
use Illuminate\Http\Request;
use App\Constants\OrderStatus;
use App\Models\ProductVariation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Api\Base\Controller;
use App\Models\Product;
use GuzzleHttp\Handler\Proxy;

class OrderController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'products'                          => 'required|array|min:1',
            'products.*.product_id'             => 'required|integer|exists:products,id',
            'products.*.product_variation_id'   => 'required|integer|exists:product_variations,id',
            'user_id'                           => 'required|integer|exists:users,id',
        ]);

        $totalPrice = 0;
        $priceData = [];

        try {
            DB::beginTransaction();
            // create order
            $order = Order::create(['user_id' => $request->user_id]);

            foreach ($request->products as $productData) {
                $product = Product::where('id', $productData['product_id'])
                    ->with('productVariations')
                    ->first();

                $productVariation = $product
                    ->productVariations
                    ->where('id', $productData['product_variation_id'])
                    ->first();

                $order->products()->attach($product->id, ['product_variation_id' => $productVariation->id]);

                // prepe price data for showing computation
                $priceData[] = [
                    $productVariation->name . '(monthly)'   => $productVariation->price,
                    $product->name . '(one-off)'            => $product->price,
                ];

                $totalPrice += $productVariation->price + $product->price;
            }

            DB::commit();
        } catch (\Throwable $th) {
            Log::error($th->getFile() . ': Line ' . $th->getLine() . ' - ' . $th->getMessage());
            DB::rollBack();

            return $this->formatResponse(null, 'Failed', 409);
        }

        return $this->formatResponse([
                'order_id'          => $order->id,
                'user_id'           => $order->user_id,
                'status'            => OrderStatus::NAMES[OrderStatus::UNDER_REVIEW],
                'price_details'     => $priceData,
                'total'             => number_format($totalPrice, 2)
            ],
            'Success', 201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        return $this->formatResponse($this->getOrderDetails($order),'Success', 201);
    }

    /**
     * Update order status the specified Order.
    *
    * @param Request $request
    * @param Order $order
    * @return void
    */
    public function updateOrderStatus(Request $request, Order $order)
    {
        $request->validate(['status' => 'required|in:1,2,3']);

        if ($order->update(['status' => $request->status])) {
            return $this->formatResponse($this->getOrderDetails($order), 'Success', 202);
        }

        return $this->formatResponse(null, 'Failed', 400);
    }

    /**
     * Removes Products form an order
     *
     * @param Request $request
     * @param Order $order
     * @return void
     */
    public function removeOrderProduct(Request $request, Order $order)
    {
        $request->validate(['order_product_id' => 'required|integer|exists:order_product,id']);

        try {
            $order->products()->wherePivot('id', $request->order_product_id)->detach();

            return $this->formatResponse($this->getOrderDetails($order), 'Success', 202);
        } catch (\Throwable $th) {
            return $this->formatResponse(null, 'This order does not have the provided order product id', 400);
        }

        return $this->formatResponse(null, 'Failed', 400);
    }

    /**
     * Add Products form an order
     *
     * @param Request $request
     * @param Order $order
     * @return void
     */

    public function addOrderProduct(Request $request, Order $order)
    {
        $request->validate([
            'product_id'            => 'required|integer|exists:products,id',
            'product_variation_id'  => 'required|integer|exists:product_variations,id',
        ]);

        try {
            $order->products()->attach($request->product_id, ['product_variation_id' => $request->product_variation_id]);

            return $this->formatResponse($this->getOrderDetails($order), 'Success', 202);
        } catch (\Throwable $th) {
            return $this->formatResponse(null, 'Failed', 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        try {
            DB::beginTransaction();

            $order->products()->detach();
            $order->delete();

            DB::commit();

            return $this->formatResponse(null, 'Sucess', 200);
        } catch (\Throwable $th) {
            DB::rollBack();
            return $this->formatResponse(null, 'Failed', 400);
        }
    }

    /**
     * geenrate an array of order details from order object
     *
     * @param Order $order
     * @return void
     */
    protected function getOrderDetails(Order $order)
    {
        $order->load('products.productVariations');
        $priceData = [];
        $totalPrice = 0;

        foreach ($order->products as $product) {
            // prepe price data for showing computation

            if (! $product->productVariations) {
                continue;
            }

            foreach ($product->productVariations as $productVariation)

            $priceData[] = [
                $productVariation->name . '(monthly)'   => $productVariation->price,
                $product->name . '(one-off)'            => $product->price,
            ];

            $totalPrice += $productVariation->price + $product->price;
        }

        return [
            'order_id'          => $order->id,
            'user_id'           => $order->user_id,
            'status'            => OrderStatus::NAMES[$order->status],
            'price_details'     => $priceData,
            'total'             => number_format($totalPrice, 2)
        ];
    }
}
