<?php

namespace Tests\Feature\Api\BasketController;

use Tests\TestCase;
use App\Models\User;
use App\Constants\UserRoles;
use Database\Seeders\DatabaseSeeder;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UpdateCustomerTest extends TestCase
{
    use DatabaseMigrations, WithFaker;

    // asserts the following:
    // - status code 202
    // - success is true
    // - message is 'Success'
    // - data is null
    public function test_api_update_customer__success()
    {
        $this->seed(DatabaseSeeder::class);

        User::factory()->create();

        $response = $this->patchJson('/api/customers/1', [
            'password'                  => 'secret123',
            'password_confirmation'     => 'secret123',

            'name'                      => $this->faker()->userName(),
            'firstname'                 => $this->faker()->firstName(),
            'lastname'                  => $this->faker()->lastName(),
        ]);

        $response->assertStatus(202)
            ->assertJson(fn (AssertableJson $json) =>
                $json->hasAll('success', 'message', 'data')
                    ->where('success', true)
                    ->where('message', 'Success')
                    ->where('data', null)
            );
    }

    // asserts the following:
    // - status code 422
    // - success is false
    // - data list all missing data
    public function test_api_udpate_customer__fail_missing_data()
    {
        $this->seed(DatabaseSeeder::class);

        User::factory()->create();

        $response = $this->patchJson('/api/customers/1', []);

        $response->assertStatus(422)
            ->assertJson(fn (AssertableJson $json) =>
                $json->hasAll('success', 'message', 'data')
                    ->where('success', false)
                    ->where('data.password', ['The password field is required.'])
                    ->where('data.name', ['The name field is required.'])
                    ->where('data.firstname', ['The firstname field is required.'])
                    ->where('data.lastname', ['The lastname field is required.'])
            );
    }

    // asserts the following:
    // - status code 404
    // - success is false
    public function test_api_update_customer__fail_error_404()
    {
        $this->seed(DatabaseSeeder::class);

        User::factory()->create();

        $response = $this->patchJson('/api/customers/99999', [
            'role_id'                   => UserRoles::CUSTOMER,
            'email'                     => $this->faker()->email(),
            'password'                  => 'secret123',
            'password_confirmation'     => 'secret',

            'name'                      => $this->faker()->userName(),
            'firstname'                 => $this->faker()->firstName(),
            'lastname'                  => $this->faker()->lastName(),
        ]);

        $response->assertStatus(404)
            ->assertJson(fn (AssertableJson $json) =>
                $json->hasAll('success', 'message', 'data')
                    ->where('success', false)
            );
    }

}
