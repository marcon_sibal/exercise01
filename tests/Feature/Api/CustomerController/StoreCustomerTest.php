<?php

namespace Tests\Feature\Api\BasketController;

use Tests\TestCase;
use App\Constants\UserRoles;
use Database\Seeders\DatabaseSeeder;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class StoreCustomerTest extends TestCase
{
    use DatabaseMigrations, WithFaker;

    // asserts the following:
    // - status code 201
    // - success is true
    // - message is 'Success'
    // - data is null
    public function test_api_add_customer__success()
    {
        $this->seed(DatabaseSeeder::class);

        $response = $this->postJson('/api/customers', [
            'email'                     => $this->faker()->email(),
            'password'                  => 'secret123',
            'password_confirmation'     => 'secret123',

            'name'                      => $this->faker()->userName(),
            'firstname'                 => $this->faker()->firstName(),
            'lastname'                  => $this->faker()->lastName(),
        ]);

        $response->assertStatus(201)
            ->assertJson(fn (AssertableJson $json) =>
                $json->hasAll('success', 'message', 'data')
                    ->where('success', true)
                    ->where('message', 'Success')
                    ->where('data', null)
            );
    }

    // asserts the following:
    // - status code 422
    // - success is false
    // - data list all missing data
    public function test_api_add_customer__fail_missing_data()
    {
        $this->seed(DatabaseSeeder::class);

        $response = $this->postJson('/api/customers', []);

        $response->assertStatus(422)
            ->assertJson(fn (AssertableJson $json) =>
                $json->hasAll('success', 'message', 'data')
                    ->where('success', false)
                    ->where('data.email', ['The email field is required.'])
                    ->where('data.password', ['The password field is required.'])
                    ->where('data.name', ['The name field is required.'])
                    ->where('data.firstname', ['The firstname field is required.'])
                    ->where('data.lastname', ['The lastname field is required.'])
            );
    }

    // asserts the following:
    // - status code 422
    // - success is false
    // - message is 'The password confirmation does not match.'
    public function test_api_add_customer__fail_password_mismatch()
    {
        $this->seed(DatabaseSeeder::class);

        $response = $this->postJson('/api/customers', [
            'email'                     => $this->faker()->email(),
            'password'                  => 'secret123',
            'password_confirmation'     => 'secret',

            'name'                      => $this->faker()->userName(),
            'firstname'                 => $this->faker()->firstName(),
            'lastname'                  => $this->faker()->lastName(),
        ]);

        $response->assertStatus(422)
            ->assertJson(fn (AssertableJson $json) =>
                $json->hasAll('success', 'message', 'data')
                    ->where('success', false)
                    ->where('message', 'The password confirmation does not match.')
                    ->where('data.password', ['The password confirmation does not match.'])
            );
    }

}
